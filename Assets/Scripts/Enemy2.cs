﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Enemy
{
	[SerializeField] private Transform shootPos1;
	[SerializeField] private Transform shootPos2;
	
	void Start()
    {
	    Setup();
	    
	    if (canMove)
	    {
		    animator.Play("E2MoveL");
	    }
    }

    void Update()
    {
	    if (canMove && moving)
	    {
		    Move();
	    }
	    
	    CheckShoot();
	    CheckMutate();
    }

    protected override void Move()
    {
	    //side reached
	    if (!reverse)
	    {
		    if (Vector2.Distance(transform.position, left) < 0.1f)
		    {
			    reverse = !reverse;
			    animator.Play("E2MoveR");
			    pathTimer = 0f;
		    }
	    }
	    else
	    {
		    if (Vector2.Distance(transform.position, right) < 0.1f)
		    {
			    reverse = !reverse;
			    animator.Play("E2MoveL");
			    pathTimer = 0f;
		    }
	    }
		
	    //move
	    if (!reverse)
	    {
		    transform.Translate(- speed * Time.deltaTime, 0, 0);
	    }
	    else
	    {
		    transform.Translate(speed * Time.deltaTime, 0, 0);
	    }

	    pathTimer += Time.deltaTime;
	    if (pathTimer > maxPathTimer)
	    {
		    if (!reverse)
		    {
			    animator.Play("E2MoveR");
		    }
		    else
		    {
			    animator.Play("E2MoveL");
		    }
		    
		    reverse = !reverse;

		    pathTimer = 0f;
	    }
    }

    //fix this!
    protected override void CheckShoot()
    {
	    coolDown += Time.deltaTime;
	    shootTimer += Time.deltaTime;
	    
	    if (coolDown > maxCooldown)
	    {
		    if (Vector2.Distance(player.transform.position, transform.position) < 5f)
		    {
			    if (!animator.GetCurrentAnimatorStateInfo(0).IsName("E2Shoot"))
			    {
				    shootTimer = 0f;
				    animator.Play("E2Shoot");
				    moving = false;
			    }
		    }
		    
		    if (shootTimer > shootTime && !shot)
		    {
			    Shoot();
			    shot = true;
		    }

		    if (shootTimer > shootTime + 0.3f)
		    {
			    coolDown = 0f;
			    shot = false;
			    moving = true;
		    }
	    }
    }

    protected override void Shoot()
    {
	    GameObject gameObject1 = Instantiate(projectilePrefab, shootPos1.position, Quaternion.identity);
	    gameObject1.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -300f));
	    GameObject gameObject2 = Instantiate(projectilePrefab, shootPos2.position, Quaternion.identity);
	    gameObject2.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -300f));
    }
}
