﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewindManager : MonoBehaviour
{
	[SerializeField] private GameObject[] leftoverPrefabs;
	private Player player;
	private Transform rewindTargetTransform;

	private float maxTimeRewindKickOff = 0.2f;
	private float rewindKickOffTimer = 0f;
	private bool rewinding = false;
	private float maxRewindTime = 1f;
	private float rewindSpeed = 1f;
	
	void Awake()
	{
		
	}

    void Start()
    {
	    player = Player.Self;
	    rewindTargetTransform = player.RewindTargetTransform;
    }

    void Update()
    {
	    Controls();
	    
	    CheckPlayer();

	    if (rewinding)
	    {
		    Rewind();
	    }
    }

    private void Controls()
    {
	    if (!rewinding)
	    {
		    if (Input.GetKeyDown(KeyCode.E))
		    {
			    DeactivatePlayer();
			    Rewind();
			    SpawnLeftovers();
		    }
	    }
    }

    private void SpawnLeftovers()
    {
	    if (player.SpriteIndex < leftoverPrefabs.Length)
	    {
		    Instantiate(leftoverPrefabs[player.SpriteIndex], player.transform.position,
			    Quaternion.identity);
	    }
	    else
	    {
		    Instantiate(leftoverPrefabs[leftoverPrefabs.Length - 1], player.transform.position,
			    Quaternion.identity);
	    }
    }

    private void CheckPlayer()
    {
	    if (!rewinding)
	    {
		    if (!player.ControlsActive)
		    {
			    rewindKickOffTimer -= Time.deltaTime;
		    }

		    if (rewindKickOffTimer <= 0)
		    {
			    DeactivatePlayer();
		    }
	    }
    }

    private void DeactivatePlayer()
    {
	    rewinding = true;
	    player.DeactivatePlayer();
	    rewindKickOffTimer = maxTimeRewindKickOff;
	    rewindSpeed = Vector2.Distance(rewindTargetTransform.position, player.transform.position) / maxRewindTime;
    }

    private void Rewind()
    {
	    Vector2 vectorToTarget = rewindTargetTransform.position - player.transform.position;
	    vectorToTarget = vectorToTarget.normalized;
	    
	    player.transform.Translate(
		    vectorToTarget.x * rewindSpeed * Time.deltaTime,
		    vectorToTarget.y * rewindSpeed * Time.deltaTime,
		    0);

	    if (Vector2.Distance(rewindTargetTransform.position, player.transform.position) < 0.1f)
	    {
		    rewinding = false;
		    player.ResetPlayerMelting();
	    }
    }
}
