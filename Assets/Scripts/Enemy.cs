﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    protected Animator animator;
    protected Vector2 left;
    protected Vector2 right;
    protected bool reverse = false;
    [SerializeField] protected bool canMove = true;
    protected Player player;
    [SerializeField] protected GameObject projectilePrefab;
    protected Rigidbody2D rigidbody2D;
    [SerializeField] protected GameObject[] levels;
    protected int levelIndex = 0;

    protected bool moving = true;
    protected float speed = 1f;

    protected float maxPathTimer = 7f;
    protected float pathTimer = 0f;

    protected float shootTimer = 0f;
    protected float shootTime = 0.4f;
    protected float maxCooldown = 2f;
    protected float coolDown = 100f;
    protected bool shot = false;

    void Awake()
    {
        animator = GetComponent<Animator>();
        left = transform.GetChild(0).position;
        right = transform.GetChild(1).position;
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    protected void Setup()
    {
        player = Player.Self;
    }

    protected void CheckMutate()
    {
        if (player.GetPercentTime() < 0.33f)
        {
            if (levelIndex != 2)
            {
                GameObject next = Instantiate(levels[2], transform.position, Quaternion.identity);
                Enemy enemy = next.GetComponent<Enemy>();
                enemy.LevelIndex = 2;
                enemy.Left = left;
                enemy.Right = right;
                Destroy(gameObject);
            }
        }
        else if (player.GetPercentTime() < 0.66f)
        {
            if (levelIndex != 1)
            {
                GameObject next = Instantiate(levels[1], transform.position, Quaternion.identity);
                Enemy enemy = next.GetComponent<Enemy>();
                enemy.LevelIndex = 1;
                enemy.Left = left;
                enemy.Right = right;
                Destroy(gameObject);
            }
        }
        else
        {
            if (levelIndex != 0)
            {
                GameObject next = Instantiate(levels[0], transform.position, Quaternion.identity);
                Enemy enemy = next.GetComponent<Enemy>();
                enemy.LevelIndex = 0;
                enemy.Left = left;
                enemy.Right = right;
                Destroy(gameObject);
            }
        }
    }

    protected abstract void Move();
    protected abstract void CheckShoot();
    protected abstract void Shoot();

    public int LevelIndex
    {
        get { return levelIndex; }
        set { levelIndex = value; }
    }

    public Vector2 Left
    {
        get => left;
        set => left = value;
    }

    public Vector2 Right
    {
        get => right;
        set => right = value;
    }
}