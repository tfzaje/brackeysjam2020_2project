﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : Enemy
{
    void Start()
    {
        Setup();
        shootTime = 2f;
    }

    void Update()
    {
        CheckShoot();
        CheckMutate();
        MaxVelocity();
    }

    protected override void Move()
    {
        
    }

    private void MaxVelocity()
    {
        if (rigidbody2D.velocity.y > 300)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 300);
        }
        else if (rigidbody2D.velocity.y < -300)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, -300);
        }
    }

    protected override void CheckShoot()
    {
        shootTimer += Time.deltaTime;

        if (shootTimer > shootTime)
        {
            Shoot();
            shootTimer = 0f;
        }
    }

    protected override void Shoot()
    {
        GameObject gameObject1 = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        gameObject1.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300f, 0));
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Ground"))
        {
            rigidbody2D.AddForce(new Vector2(0, 300));
        }
    }
}
