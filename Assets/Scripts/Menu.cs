﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    private static Menu self;
    [SerializeField] private GameObject winMenuUI;
    [SerializeField] private GameObject pauseMenuUI;
    private Text time;
    private Text respawnCount;
    private Player player;
    private int respawnCounter = 0;
    
    void Start()
    {
        time = gameObject.transform.GetChild(3).GetComponent<Text>();
        respawnCount = gameObject.transform.GetChild(2).GetComponent<Text>();
        player = Player.Self;
    }

    void Update()
    {
        time.text = player.LiveSecs.ToString();
        respawnCounter = player.NumberOfReplacements - player.Replaced;
        respawnCount.text = respawnCounter.ToString();
    }
    void Awake()
    {
        self = this;
    }

    public static Menu GetSelf()
    {
        return self;
    }

    public void SetPauseMenuAktive()
    {
        pauseMenuUI.SetActive(true);
    }

    public void SetPauseMenuInaktive()
    {
        pauseMenuUI.SetActive(false);
    }

    public void SetWinMenuAktive()
    {
        winMenuUI.SetActive(true);
    }

    public void HomeMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void NextLevel(int scenenNumber)
    {
        SceneManager.LoadScene(scenenNumber);
    }

    public void TotalRewind()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
