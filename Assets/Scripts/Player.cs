﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	private static Player self;

	[SerializeField] private Sprite[] playerSprites;
	[SerializeField] private Sprite deathSprite;

	private Rigidbody2D rigidbody;
	private Camera cam;
	private Transform rewindTargetTransform;
	private SpriteRenderer spriteRenderer;
	private Collider2D collider;
	[SerializeField] private LayerMask groundMask;
	private int numberOfReplacements = 5;
	private int replaced = 0;
	[SerializeField] private bool isHomeMenuAnimation = false;

	private float maxXVelocity = 10f;
	private float groundVelocity = 10f;
	private float airVelocity = 1f;
	
	private bool movementKeyWasPressed = false;
	private float maxDecelerationSecs = 5f;
	private float decelerationSecs;

	private bool onGround = false;
	// private float airResistance = 1.0f;

	private float maxLiveSecs = 60f;
	private float liveSecs;
	private bool controlsActive = true;
	int spriteIndex;

	[SerializeField] private int scenenNumber;

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		if (isHomeMenuAnimation)
			return;
		
		rigidbody = GetComponent<Rigidbody2D>();
		collider = GetComponent<Collider2D>();
		cam = Camera.main;
		rewindTargetTransform = GameObject.FindWithTag("RewindTarget").transform;
		self = this;
		decelerationSecs = maxDecelerationSecs;
		liveSecs = maxLiveSecs;
	}

    void Start()
    {

    }

    void Update()
    {
	    if (!isHomeMenuAnimation)
	    {
		    movementKeyWasPressed = false;
	    
		    FollowingCamera();

		    CheckGround();
	    
		    PlayerControls();

		    // EnforceAirResistance();

		    EnforceMaxVelocity();

		    Decelerate();
	    }

	    HandlePlayerMelting();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
	    if (other.gameObject.CompareTag("Projectile"))
	    {
		    liveSecs -= other.GetComponent<Projectile>().Damage;
		    Destroy(other.gameObject);
	    }

		if (other.gameObject.CompareTag("Goal"))
		{
			SceneManager.LoadScene(scenenNumber);
		}
    }

    private void OnTriggerStay2D(Collider2D other)
    {
	    if (other.gameObject.CompareTag("Sun"))
	    {
		    Debug.Log("sun");
		    ReduceLive();
		    ReduceLive();
		    ReduceLive();
		    ReduceLive();
	    }
    }

    private void EnforceAirResistance()
    {
	    // Vector2 velocity = rigidbody.velocity;
	    //
	    // if (velocity.x > 0)
	    // {
		   //  if (velocity.x < airResistance)
		   //  {
			  //   rigidbody.AddForce(new Vector2(-velocity.x, 0));
		   //  }
		   //  else
		   //  {
			  //   rigidbody.AddForce(new Vector2(-airResistance, 0));
		   //  }
	    // }
	    // else
	    // {
		   //  if (velocity.x > -airResistance)
		   //  {
			  //   rigidbody.AddForce(new Vector2(-velocity.x, 0));
		   //  }
		   //  else
		   //  {
			  //   rigidbody.AddForce(new Vector2(airResistance, 0));
		   //  }
	    // }
    }

    private void HandlePlayerMelting()
    {
	    if (controlsActive)
	    {
		    ReduceLive();

		    if (liveSecs > 0)
		    {
			    float secsPerSprite = maxLiveSecs / playerSprites.Length;
			    float elapsedSecs = maxLiveSecs - liveSecs;
			    spriteIndex = (int) (elapsedSecs / secsPerSprite);

			    spriteRenderer.sprite = playerSprites[spriteIndex]; 
		    }
		    else
		    {
			    spriteRenderer.sprite = deathSprite;
			    controlsActive = false;
		    }
	    }
    }

    private void ReduceLive()
    {
	    liveSecs -= Time.deltaTime;
    }

    private void CheckGround()
    {
	    RaycastHit2D hit = Physics2D.BoxCast(new Vector2(transform.position.x, transform.position.y), new Vector2(1f, 0.5f), 0,
		    Vector2.down, 0.45f, groundMask);

	    if (hit.collider != null)
	    {
		    if (hit.collider.CompareTag("Ground"))
		    {
			    onGround = true;
			    return;
		    }
	    }

	    onGround = false;
    }

    private void Decelerate()
    {
	    if (!movementKeyWasPressed)
	    {
		    if (Time.deltaTime > decelerationSecs)
		    {
			    rigidbody.velocity = new Vector2(0, rigidbody.velocity.y);
			    return;
		    }
		    
		    float horizontalVelocity = rigidbody.velocity.x;
		    float decelerationThisFrame = (horizontalVelocity / decelerationSecs) * Time.deltaTime;
		    decelerationSecs -= Time.deltaTime;
		    
		    rigidbody.velocity = new Vector2(horizontalVelocity - decelerationThisFrame, rigidbody.velocity.y);
	    }
	    else
	    {
		    decelerationSecs = maxDecelerationSecs;
	    }
    }

    private void PlayerControls()
    {
	    if (!controlsActive)
		    return;

	    if (Input.GetKeyDown(KeyCode.F))
	    {
		    SetRewindPosition();
	    }
	    
	    if (Input.GetKeyDown(KeyCode.Space))
	    {
		    if (onGround)
		    {
			    rigidbody.AddForce(new Vector2(0, 300));
		    }
	    }
	    
	    if (Input.GetKey(KeyCode.D))
	    {
		    if (onGround)
		    {
			    rigidbody.AddForce(new Vector2(groundVelocity, 0));
		    }
		    else
		    {
			    rigidbody.AddForce(new Vector2(airVelocity, 0));
		    }
		    
		    movementKeyWasPressed = true;
	    }
	    
	    if (Input.GetKey(KeyCode.A))
	    {
		    if (onGround)
		    {
			    rigidbody.AddForce(new Vector2(- groundVelocity, 0));
		    }
		    else
		    {
			    rigidbody.AddForce(new Vector2(- airVelocity, 0));
		    }
		    movementKeyWasPressed = true;
	    }
    }

    private void FollowingCamera()
    {
	    cam.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }

    private void EnforceMaxVelocity()
    {
	    if (rigidbody.velocity.x > maxXVelocity)
	    {
		    rigidbody.velocity = new Vector2(maxXVelocity, rigidbody.velocity.y);
	    }

	    if (rigidbody.velocity.x < -maxXVelocity)
	    {
		    rigidbody.velocity = new Vector2(-maxXVelocity, rigidbody.velocity.y);
	    }
    }

    private void SetRewindPosition()
    {
	    if (replaced < numberOfReplacements)
	    {
		    rewindTargetTransform.position = transform.position;
		    replaced++;
	    }
    }

    public void ResetPlayerMelting()
    {
	    liveSecs = maxLiveSecs;

	    spriteRenderer.sprite = playerSprites[0];

	    controlsActive = true;

	    collider.enabled = true;
	    rigidbody.simulated = true;
    }

    public void DeactivatePlayer()
    {
	    controlsActive = false;
	    collider.enabled = false;
	    rigidbody.velocity = Vector2.zero;
	    rigidbody.simulated = false;
    }

    public float GetPercentTime()
    {
	    return liveSecs / maxLiveSecs;
    }

    public static Player Self => self;

    public float LiveSecs => liveSecs;

    public float MaxLiveSecs => maxLiveSecs;

    public int SpriteIndex => spriteIndex;

    public bool ControlsActive => controlsActive;

    public Transform RewindTargetTransform => rewindTargetTransform;

	public int NumberOfReplacements => numberOfReplacements;

	public int Replaced => replaced;
}
