﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	[SerializeField] private float damage;
	private Player player;

	private void Start()
	{
		player = Player.Self;
	}

	private void Update()
	{
		if (Vector2.Distance(player.transform.position, transform.position) > 500f)
		{
			Destroy(gameObject);
		}
	}

	public float Damage => damage;
}
